/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

/**
 *
 * @author root
 */
public class QuickSort {
    public static void main(String[] args) {


        /*QUICKSORT: Es un algoritmo basado en la técnica de divide y vencerás, que permite, en promedio, ordenar n elementos
    en un tiempo proporcional a n log n. Quicksort es actualmente el más eficiente y veloz de los métodos de ordenación interna.*/
        int[] arreglo = {9, 5, 6, 2, 1, 0, 8, 7, 3, 4};

        imprimeArreglo(arreglo);

        quicksort(arreglo, 0, 9);

        imprimeArreglo(arreglo);

    }

    public static void imprimeArreglo(int[] arreglo) {

        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + " ");
        }
        System.out.println("");
        System.out.println("___________________");

    }

    public static void quicksort(int[] arreglo, int izq, int der) {


        int pivote = arreglo[izq];


        int i = izq;
        int j = der;

        int swap;


        while (i < j) {

            while (arreglo[i] <= pivote && i < j) {
                i++;
            }
   
            while (arreglo[j] > pivote) {
                j--;
            }
            
            if (i < j) {
                swap = arreglo[i];
                arreglo[i] = arreglo[j];
                arreglo[j] = swap;

                imprimeArreglo(arreglo);
            }
        }
        arreglo[izq] = arreglo[j];
        arreglo[j] = pivote;

        if (izq < j - 1) {
            quicksort(arreglo, izq, j - 1);
        }
        if (j + 1 < der) {
            quicksort(arreglo, j + 1, der);
        }

    }
}
