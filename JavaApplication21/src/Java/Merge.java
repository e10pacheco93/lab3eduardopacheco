/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

import java.util.Arrays;

/**
 *
 * @author root
 */
public class Merge {

/*MERGE SORT: El método MergeSort es un algoritmo de ordenación recursivo con un número de comparaciones entre 
elementos del array mínimo.Su funcionamiento es similar al Quicksort, y está basado en la técnica divide y vencerás.*/


    public static void main(String[] args) {
        Integer[] itens = {2,6,4,9,1,3,8,7,0};
         System.out.println("LISTA DESORDENADA : "+Arrays.toString(itens));
        

        Integer[] tmp = new Integer[itens.length];
        int Izquierda = 0;
        int Derecha = itens.length - 1;
        
        mergeSort(itens, tmp, Izquierda, Derecha);
        System.out.println("LISTA ORDENADA: "+Arrays.toString(itens));
        
    }

    private static void mergeSort(Integer[] itens, Integer[] tmpArray, int Izquierda, int Derecha) {

        if(itens == null || itens.length == 0 || Izquierda >= Derecha){
            return;
        }

        int midle = (Izquierda + Derecha) / 2;

        mergeSort(itens, tmpArray, Izquierda, midle);
        mergeSort(itens, tmpArray, midle + 1, Derecha);

        merge(itens, tmpArray, Izquierda, midle + 1, Derecha);
    }

    private static void merge(Integer[] itens, Integer[] tmpArray, int Izquierda, int Derecha, int rightEnd) {
        int leftEnd = Derecha- 1;
        int tmpIndex = Izquierda;

        while (Izquierda <= leftEnd && Derecha<= rightEnd){
            if (itens[Izquierda] < itens[Derecha] ){
                tmpArray[tmpIndex++] = itens[Izquierda++];
            } else {
                tmpArray[tmpIndex++] = itens[Derecha++];
            }
        }

        while (Izquierda <= leftEnd) { // Copiar el resto de la mitad IZQUIERDA
            tmpArray[tmpIndex++] = itens[Izquierda++];
        }
        while (Derecha <= rightEnd) { // Copiar el resto de la mitad DERECHA
            tmpArray[tmpIndex++] = itens[Derecha++];
        }
        while(rightEnd >= 0){ // Copiar TEMP de nuevo
            itens[rightEnd] = tmpArray[rightEnd--];
        }
    }
}
