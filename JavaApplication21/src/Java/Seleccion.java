/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

/**
 *
 * @author root
 */
public class Seleccion {
    
    
    public static void ordenar(int[] arreglo) {

        /*METODO SELECCION: El método de ordenación por selección consiste en buscar el elemento
          más pequeño del array y se coloca en la primera posición. Entre los 
          restantes, se busca el elemento más pequeño y se coloca en la segunda
           posición. Entre los restantes se busca el elemento más pequeño y se 
            coloca en la tercera posición.*/

        int c = 1;
        for (int i = 0; i < arreglo.length - 1; i++) {
            int min = i;
            for (int j = i + 1; j < arreglo.length; j++) {

                c++;
                if (arreglo[j] < arreglo[min]) {
                    min = j;
                }
            }
            if (i != min) {
                int aux = arreglo[i];
                arreglo[i] = arreglo[min];
                arreglo[min] = aux;
            }
        }
    }

    public static void imprimirLista(int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            System.out.print(arreglo[i] + ",");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int lista[] = {9, 1, 4, 2, 8, 3, 5, 6, 7, 0};
        System.out.print("Lista desordenada: ");
        imprimirLista(lista);

        ordenar(lista);

        System.out.print("Lista ordenada: ");
        imprimirLista(lista);
    }
}
