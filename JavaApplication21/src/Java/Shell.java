/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java;

import java.util.Scanner;
import javax.swing.JOptionPane;


/**
 *
 * @author root
 */
public class Shell {
        
    
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int cantidad = Integer.parseInt(JOptionPane.showInputDialog(" Digite el tamaño del arreglo: "));
        int[] arreglo = new int[cantidad];

        for (int i = 0; i < cantidad; i++) {
            System.out.print(" Digite el " + (i + 1) + " numero: ");

            arreglo[i] = Integer.parseInt(JOptionPane.showInputDialog(" Digite el tamaño del arreglo: "));
        }

        shell(arreglo);
        scan.close();
        for (int i : arreglo) {
            System.out.println(i);
        }
    }

    static void shell(int[] arreglo) {
        int inta, i, aux;
        boolean band;
        inta = arreglo.length;
        while (inta > 0) {
            inta = inta / 2;
            band = true;
            while (band) {
                band = false;
                i = 0;
                while ((i + inta) <= arreglo.length - 1) {
                    if (arreglo[i] > arreglo[i + inta]) {
                        aux = arreglo[i];
                        arreglo[i] = arreglo[i + inta];
                        arreglo[i + inta] = aux;
                        band = true;
                    }
                    i = i + 1;
                }
            }
        }

    }
}
